﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using Application = System.Windows.Application;
using ContextMenu = System.Windows.Controls.ContextMenu;
using MenuItem = System.Windows.Controls.MenuItem;

namespace Lab_2
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private static void DeleteFileOnClick(object sender, RoutedEventArgs e)
        {
            if (sender == null) return;
            var tvi = ((ContextMenu) ((MenuItem) sender).Parent).PlacementTarget as TreeViewItem;
            var path = Path.Combine((string) tvi.Tag, (string) tvi.Header);
            var attr = File.GetAttributes(path);

            if (attr.HasFlag(FileAttributes.ReadOnly))
                File.SetAttributes(path, attr & ~FileAttributes.ReadOnly);

            File.Delete(path);
            (tvi.Parent as TreeViewItem)?.Items.Remove(tvi);
        }

        private void CreateFileOnClick(object sender, RoutedEventArgs e)
        {
            if (sender == null) return;
            var tvi = ((ContextMenu) ((MenuItem) sender).Parent).PlacementTarget as TreeViewItem;
            //Creating a second window
            var wnd = new Create();
            wnd.ShowDialog();

            if (wnd.FileName == null) return;
            var path = Path.Combine((string) tvi.Tag, (string) tvi.Header);
            var newFilePath = Path.Combine(path, wnd.FileName);

            if (wnd.Directory)
                Directory.CreateDirectory(newFilePath);
            else
                File.Create(newFilePath);

            FileAttributes attr = 0;
            if (wnd.aArchive)
                attr |= FileAttributes.Archive;
            if (wnd.aReadOnly)
                attr |= FileAttributes.ReadOnly;
            if (wnd.aHidden)
                attr |= FileAttributes.Hidden;
            if (wnd.aSystem)
                attr |= FileAttributes.System;

            File.SetAttributes(newFilePath, attr);

            var item = new TreeViewItem
            {
                Header = wnd.FileName,
                Tag = path,
                ContextMenu = wnd.Directory ? CreatePathCtx() : CreateFileCtx()
            };


            tvi.Items.Add(item);
        }

        private void DelFolderOnClick(object sender, RoutedEventArgs e)
        {
            if (sender == null) return;
            var tvi = ((ContextMenu) ((MenuItem) sender).Parent).PlacementTarget as TreeViewItem;
            var path = Path.Combine((string) tvi.Tag, (string) tvi.Header);

            RemoveDir(new DirectoryInfo(path));
            Directory.Delete(path);

            if (tvi.Parent is TreeViewItem item)
                item.Items.Remove(tvi);
            else
                treeView.Items.Remove(tvi);
        }

        private static void RemoveDir(DirectoryInfo dir)
        {
            foreach (var item in dir.EnumerateFileSystemInfos())
            {
                if (item is DirectoryInfo info)
                {
                    RemoveDir(info);
                }
                else
                {
                    var attr = item.Attributes;
                    if (attr.HasFlag(FileAttributes.ReadOnly))
                        item.Attributes = attr & ~FileAttributes.ReadOnly;
                }

                item.Delete();
            }
        }

        //Open from toolbar
        private void OpenItemOnClick(object sender, RoutedEventArgs e)
        {
            var dlg = new FolderBrowserDialog
            {
                Description = "Select directory to open"
            };

            if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
            var root = new TreeViewItem
            {
                Header = Path.GetFileName(dlg.SelectedPath),
                Tag = Path.GetDirectoryName(dlg.SelectedPath),
                ContextMenu = CreatePathCtx()
            };

            var dir = new DirectoryInfo(dlg.SelectedPath);
            RetrieveFiles(root, dir);

            treeView.Items.Add(root);
        }

        private void ExitOnClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void RetrieveFiles(TreeViewItem root, DirectoryInfo dir)
        {
            foreach (var item in dir.EnumerateFileSystemInfos())
            {
                var tvi = new TreeViewItem
                {
                    Header = item.Name,
                    Tag = Path.GetDirectoryName(item.FullName)
                };
                //If dir then add to it context and recurvivle call again
                if (item is DirectoryInfo info)
                {
                    tvi.ContextMenu = CreatePathCtx();
                    RetrieveFiles(tvi, info);
                }
                else
                {
                    tvi.ContextMenu = CreateFileCtx();
                }

                root.Items.Add(tvi);
            }
        }

        //Assigns Proper function and creates menuItem, context and then adds as new item for right click in TreeView
        private ContextMenu CreatePathCtx()
        {
            var create = new MenuItem
            {
                Header = "Create"
            };
            //Adding funciton to event
            create.Click += CreateFileOnClick;

            var delete = new MenuItem
            {
                Header = "Delete"
            };
            delete.Click += DelFolderOnClick;

            var ctx = new ContextMenu();
            ctx.Items.Add(create);
            ctx.Items.Add(delete);
            return ctx;
        }

        //Only delete here cause we dont want to add "files on files"
        private ContextMenu CreateFileCtx()
        {
            var delete = new MenuItem
            {
                Header = "Delete"
            };
            delete.Click += DeleteFileOnClick;

            var ctx = new ContextMenu();
            ctx.Items.Add(delete);
            return ctx;
        }
    }
}